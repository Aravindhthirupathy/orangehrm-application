package BaseClass;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BaseClass6 {
	
	@FindBy(xpath = "//span[.='Admin']" )
	private WebElement admin;
	
	@FindBy(xpath = "//span[normalize-space()='User Management']" )
	private WebElement usermanagement;
	
	@FindBy(xpath = "//a[normalize-space()='Users']" )
	private WebElement users;
	
	@FindBy(xpath = "//input[@placeholder='Type for hints...']")
	private WebElement employeename;
	
	@FindBy(xpath = "(//div[.='Anthony  Nolan'])[3]")
	private WebElement optionclick;
	
	@FindBy(xpath = "//button[.=' Search ']")
	private WebElement search;
	
	@FindBy(xpath = "//span[.='(1) Record Found']")
	private WebElement onerecordfound;
	
	
	public void clickadminbutton()
	{
		admin.click();
	}
	
	public void clickusermanagement()
	{
		usermanagement.click();
	}
	
	public void clickusers()
	{
		users.click();
	}
	
	public void employeenametextboxname(String name)
	{
		employeename.sendKeys(name);
	}
	
	public void clickoption()
	{
		optionclick.click();
	}
	
	public void searchbutton()
	{
		search.click();
	}
	
	
	public boolean onerecordwasfound()
	{
		if (onerecordfound.isDisplayed()) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	

}
