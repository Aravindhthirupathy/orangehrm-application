package BaseClass;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BaseClass8 {
	
	@FindBy(xpath = "//span[.='Admin']" )
	private WebElement admin;
	
	@FindBy(xpath = "//span[normalize-space()='User Management']" )
	private WebElement usermanagement;
	
	@FindBy(xpath = "//a[normalize-space()='Users']" )
	private WebElement users;
	
	@FindBy(xpath = "//button[@class='oxd-icon-button']" )
	private WebElement uparrow;
	
	@FindBy(xpath = "//div[@class='oxd-input-group__label-wrapper']")
	private WebElement minimized;
	
	public void clickadminbutton()
	{
		admin.click();
	}
	
	public void clickusermanagement()
	{
		usermanagement.click();
	}
	
	public void uparrowclick()
	{
		uparrow.click();
	}
	
	
	public boolean alloptionsminimized()
	{
		if (minimized.isDisplayed()) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	

}
