package BaseClass;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BaseClass1 {
	
	//Declaration
	@FindBy(xpath = "//h6[@class='oxd-text oxd-text--h6 oxd-topbar-header-breadcrumb-module']")
	private WebElement dashboard;
	
	//Utilization
	public boolean dashboarddisplayed()
	{
		if (dashboard.isDisplayed()) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	

}
