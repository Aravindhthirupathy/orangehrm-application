package BaseClass;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BaseClass2 {
	
	@FindBy(name = "username")
	private WebElement usnm;
	
	@FindBy(name = "password")
	private WebElement  pwd;
	
	@FindBy(xpath = "//button[.=' Login ']")
	private WebElement login;
	
	@FindBy(xpath = "//p[@class='oxd-text oxd-text--p oxd-alert-content-text']")
	private WebElement invalidcredential;
	
	public void enterusername(String username)
	{
		usnm.sendKeys(username);
	}
	
	public void enterpassword(String password)
	{
		pwd.sendKeys(password);
	}
	
	public void clickloginbutton()
	{
		login.click();
	}
	
	public boolean invalidcredentialdisplayed()
	{
		if (invalidcredential.isDisplayed()) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	

}
