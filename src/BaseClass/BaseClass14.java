package BaseClass;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BaseClass14 {
	
	
	@FindBy(xpath = "//span[.='Admin']" )
	private WebElement admin;
	
	@FindBy(xpath = "//span[normalize-space()='User Management']" )
	private WebElement usermanagement;
	
	@FindBy(xpath = "//a[normalize-space()='Users']" )
	private WebElement users;
	
	@FindBy(xpath = "//span[@class='oxd-userdropdown-tab']")
	private WebElement profile;
	
	@FindBy (xpath = "//a[.='Logout']")
	private WebElement logout;
	
	@FindBy(xpath = "//div[@class='orangehrm-login-branding']")
	private WebElement loginpage;
	
	
	public void clickadminbutton()
	{
		admin.click();
	}
	
	public void clickusermanagement()
	{
		usermanagement.click();
	}
	
	public void clickusers()
	{
		users.click();
	}
	
	public void profilebutton()
	{
		profile.click();
	}
	
	public void logoutlink()
	{
		logout.click();
	}
	
	public boolean loginpageisdispayed()
	{
		if (loginpage.isDisplayed()) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}



}
