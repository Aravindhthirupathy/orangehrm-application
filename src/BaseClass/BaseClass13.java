package BaseClass;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BaseClass13 {
	
	@FindBy(name = "username")
	private WebElement usnm;
	
	@FindBy(name = "password")
	private WebElement  pwd;
	
	@FindBy(xpath = "//button[.=' Login ']")
	private WebElement login;
	
	@FindBy(xpath = "//span[.='Admin']" )
	private WebElement admin;
	
	@FindBy(xpath = "//span[normalize-space()='User Management']" )
	private WebElement usermanagement;
	
	@FindBy(xpath = "//a[normalize-space()='Users']" )
	private WebElement users;
	
	@FindBy(xpath = "//a[.='OrangeHRM, Inc']")
	private WebElement orangehrmlink;
	
	@FindBy(xpath = "//a[@class='navbar-brand nav-logo']")
	private WebElement orangehrmlogo;
	
	
	public void enterusername(String username)
	{
		usnm.sendKeys(username);
	}
	
	public void enterpassword(String password)
	{
		pwd.sendKeys(password);
	}
	
	public void clickloginbutton()
	{
		login.click();
	}
	
	public void clickadminbutton()
	{
		admin.click();
	}
	
	public void clickusermanagement()
	{
		usermanagement.click();
	}
	
	public void clickusers()
	{
		users.click();
	}
	
	public void orangehrmlinkclick()
	{
		orangehrmlink.click();
	}
	
	
	public boolean orangehrmlogodispayed()
	{
		if (orangehrmlogo.isDisplayed()) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}


}
