package BaseClass;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BaseClass11 {
	
	@FindBy(xpath = "//span[.='Admin']" )
	private WebElement admin;
	
	@FindBy(xpath = "//span[normalize-space()='User Management']" )
	private WebElement usermanagement;
	
	@FindBy(xpath = "//a[normalize-space()='Users']" )
	private WebElement users;
	
	@FindBy(xpath = "(//button[@type='button'])[8]")
	private WebElement edit;
	
	
	@FindBy(xpath = "//h6[@class='oxd-text oxd-text--h6 orangehrm-main-title']")
	private WebElement edituser;
	
	
	public void clickadminbutton()
	{
		admin.click();
	}
	
	public void clickusermanagement()
	{
		usermanagement.click();
	}
	
	public void clickusers()
	{
		users.click();
	}
	
	public void clickedit()
	{
		edit.click();
	}
	
	
	public boolean edituserpage()
	{
		if (edituser.isDisplayed()) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}
