package BaseClass;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BaseClass7 {
	
	
	@FindBy(xpath = "//span[.='Admin']" )
	private WebElement admin;
	
	@FindBy(xpath = "//span[normalize-space()='User Management']" )
	private WebElement usermanagement;
	
	@FindBy(xpath = "//a[normalize-space()='Users']" )
	private WebElement users;
	
	@FindBy(xpath = "(//div[contains(text(),'-- Select --')])[2]")
	private WebElement status;
	
	@FindBy(xpath = "//span[.='Enabled']")
	private WebElement optionone;
	
	@FindBy(xpath = "//button[.=' Search ']")
	private WebElement search;
	
	@FindBy(xpath = "//span[.=' (2) Records Found']")
	private WebElement sixtyrecords;
	
	public void clickadminbutton()
	{
		admin.click();
	}
	
	public void clickusermanagement()
	{
		usermanagement.click();
	}
	
	public void clickusers()
	{
		users.click();
	}
	
	public void statusbutton()
	{
		status.click();
	}
	
	public void optionbuttonclick()
	{
		optionone.click();
	}
	
	public void searchbutton()
	{
		search.click();
	}
	
	
	public boolean sevenrecordfound()
	{
		if (sixtyrecords.isDisplayed()) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	

}
