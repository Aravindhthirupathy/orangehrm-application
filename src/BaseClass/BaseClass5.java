package BaseClass;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BaseClass5 {
	
	@FindBy(xpath = "//span[.='Admin']" )
	private WebElement admin;
	
	@FindBy(xpath = "//span[normalize-space()='User Management']" )
	private WebElement usermanagement;
	
	@FindBy(xpath = "//a[normalize-space()='Users']" )
	private WebElement users;
	
	@FindBy(xpath = "(//div[@class='oxd-select-text oxd-select-text--active'])[1]")
	private WebElement userrole;
	
	@FindBy(xpath = "//div[.='Admin']")
	private WebElement optionone;
	
	@FindBy(xpath = "//button[.=' Search ']")
	private WebElement search;
	
	@FindBy(xpath = "(//span[@class='oxd-text oxd-text--span'])[1]")
	private WebElement sevenrecords;
	
	
	public void clickadminbutton()
	{
		admin.click();
	}
	
	public void clickusermanagement()
	{
		usermanagement.click();
	}
	
	public void clickusers()
	{
		users.click();
	}
	
	public void userrolebutton()
	{
		userrole.click();
	}
	
	public void optionbuttonclick()
	{
		optionone.click();
	}
	
	public void searchbutton()
	{
		search.click();
	}
	
	
	public boolean sevenrecordfound()
	{
		if (sevenrecords.isDisplayed()) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
	

}
