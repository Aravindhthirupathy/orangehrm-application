package BaseClass;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BaseClass9 {
	
	@FindBy(xpath = "//span[.='Admin']" )
	private WebElement admin;
	
	@FindBy(xpath = "//span[normalize-space()='User Management']" )
	private WebElement usermanagement;
	
	@FindBy(xpath = "//a[normalize-space()='Users']" )
	private WebElement users;
	
	@FindBy(xpath = "(//input[@class='oxd-input oxd-input--active'])[2]")
	private WebElement usernametextbox;
	
	@FindBy(xpath = "//button[normalize-space()='Reset']")
	private WebElement reset;
	
	@FindBy(xpath = "(//input[@class='oxd-input oxd-input--active'])[2]")
	private WebElement emptyusernamebox;
	
	
	public void clickadminbutton()
	{
		admin.click();
	}
	
	public void clickusermanagement()
	{
		usermanagement.click();
	}
	
	public void clickusers()
	{
		users.click();
	}
	
	public void usernametextboxname(String name)
	{
		usernametextbox.sendKeys(name);
	}
	
	public void resetbutton()
	{
		reset.click();
	}
	
	
	public boolean isusernametextboxempty()
	{
		if (emptyusernamebox.equals("JasonSanjay")) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	

}
