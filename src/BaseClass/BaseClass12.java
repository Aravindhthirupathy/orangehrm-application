 package BaseClass;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BaseClass12 {
	
	@FindBy(xpath = "//span[.='Admin']" )
	private WebElement admin;
	
	@FindBy(xpath = "//span[normalize-space()='User Management']" )
	private WebElement usermanagement;
	
	@FindBy(xpath = "//a[normalize-space()='Users']" )
	private WebElement users;
	
	@FindBy(xpath = "(//span[@class='oxd-checkbox-input oxd-checkbox-input--active --label-right oxd-checkbox-input'])[2]")
	private WebElement checkbox;
	
	
	@FindBy(xpath = "//button[@class='oxd-button oxd-button--medium oxd-button--label-danger orangehrm-horizontal-margin']")
	private WebElement deleteselected;
	
	
	public void clickadminbutton()
	{
		admin.click();
	}
	
	public void clickusermanagement()
	{
		usermanagement.click();
	}
	
	public void clickusers()
	{
		users.click();
	}
	
	public void clickcheckbox()
	{
		checkbox.click();
	}
	
	
	public boolean deleteselecteddisplayed()
	{
		if (deleteselected.isDisplayed()) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}


}
