package TestScript;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import BaseClass.BaseClass2;
import BaseClass.BaseClass1;

public class TestCase2 {
	
	static {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver_win32\\chromedriver.exe");
	}
	
@Test
	
	public void loginaction()
	{
		WebDriver driver= new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");
		
		BaseClass2 loginperform = new BaseClass2();
		PageFactory.initElements(driver, loginperform);
		loginperform.enterusername("Admi");
		loginperform.enterpassword("admin123");
		loginperform.clickloginbutton();
		Assert.assertTrue(loginperform.invalidcredentialdisplayed());
		driver.close();
	}

}
