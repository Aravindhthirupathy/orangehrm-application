package TestScript;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import BaseClass.BaseClass1;
import BaseClass.BaseClass10;
import BaseClass.BaseClass11;
import BaseClass.BaseClass12;
import BaseClass.BaseClass13;
import BaseClass.BaseClass14;
import BaseClass.BaseClass2;
import BaseClass.BaseClass3;
import BaseClass.BaseClass4;
import BaseClass.BaseClass5;
import BaseClass.BaseClass6;
import BaseClass.BaseClass7;
import BaseClass.BaseClass8;
import BaseClass.BaseClass9;


public class TestCase {
	static {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver_win32\\chromedriver.exe");
	}
	
	WebDriver driver;
	
	@BeforeTest
	void beforeStart() throws IOException 
	{
		//Fileinputstream class is useful to read data from a file.
		//Creates an input file stream to read from the specified file object.
		FileInputStream fis = new FileInputStream("./commondata/orangehrmCD.txt");
		//In properties object contains key and value pair both as a string values.
		Properties p = new Properties();
		p.load(fis);
		String p1 = p.getProperty("URL");
		String p2 = p.getProperty("UN");
		String p3 = p.getProperty("PW");
		
		driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		driver.get(p1);
		driver.findElement(By.name("username")).sendKeys(p2);
		driver.findElement(By.name("password")).sendKeys(p3);
		driver.findElement(By.xpath("//button[.=' Login ']")).click();
		
	}
	
	
	@Test
	public void a()
	{
		
		BaseClass1 loginperform = new BaseClass1();
		//Pagefactory is class provided by the selenium webdriver to support page object design patterns.
		//In page factory,testers use @findby annotation used in page factory to locate and declare webelements using different locators.
		//The initElements method is used to initialize web elements
		//Initialization
		PageFactory.initElements(driver, loginperform);
		Assert.assertTrue(loginperform.dashboarddisplayed());
		
	}
	

	@Test
	public void c()
	{
		
		BaseClass3 loginperform = new BaseClass3();
		PageFactory.initElements(driver, loginperform);
		loginperform.clickadminbutton();
		loginperform.clickusermanagement();
		loginperform.clickusers();
		Assert.assertTrue(loginperform.adminpagetitledisplayed());
		
	}
	
	@Test
	public void d()
	{
		
		BaseClass4 loginperform = new BaseClass4();
		PageFactory.initElements(driver, loginperform);
		loginperform.clickadminbutton();
		loginperform.clickusermanagement();
		loginperform.clickusers();
		loginperform.usernametextboxname("JasonSanjay");
		loginperform.searchbutton();
		Assert.assertTrue(loginperform.norecordfound());
		
	}
	
	@Test
	public void e()
	{
		
		BaseClass5 loginperform = new BaseClass5();
		PageFactory.initElements(driver, loginperform);
		loginperform.clickadminbutton();
		loginperform.clickusermanagement();
		loginperform.clickusers();
		loginperform.userrolebutton();
		loginperform.optionbuttonclick();
		loginperform.searchbutton();
		Assert.assertTrue(loginperform.sevenrecordfound());
		
	}
	
	@Test
	public void f() 
	{
		
		BaseClass6 loginperform = new BaseClass6();
		PageFactory.initElements(driver, loginperform);
		loginperform.clickadminbutton();
		loginperform.clickusermanagement();
		loginperform.clickusers();
		loginperform.employeenametextboxname("Anthony Nolan");
		loginperform.clickoption();
		loginperform.searchbutton();
		Assert.assertTrue(loginperform.onerecordwasfound());
		
	}
	
	@Test
	public void g() throws IOException
	{
		
		BaseClass7 loginperform = new BaseClass7();
		PageFactory.initElements(driver, loginperform);
		loginperform.clickadminbutton();
		loginperform.clickusermanagement();
		loginperform.clickusers();
		loginperform.statusbutton();
		loginperform.optionbuttonclick();
		loginperform.searchbutton();
		Assert.assertFalse(loginperform.sevenrecordfound());
		
		
	}
	
	@Test
	public void h() throws IOException
	{
		
		BaseClass8 loginperform = new BaseClass8();
		PageFactory.initElements(driver, loginperform);
		loginperform.clickadminbutton();
		loginperform.clickusermanagement();
		loginperform.uparrowclick();
		TakesScreenshot ts = (TakesScreenshot) driver;
		File from = ts.getScreenshotAs(OutputType.FILE);
		File to = new File("./screenshot/ss1.png");
		FileUtils.copyFileToDirectory(from, to);
		Assert.assertTrue(loginperform.alloptionsminimized());
		
	}
	
	@Test
	public void i()
	{
		
		BaseClass9 loginperform = new BaseClass9();
		PageFactory.initElements(driver, loginperform);
		loginperform.clickadminbutton();
		loginperform.clickusermanagement();
		loginperform.clickusers();
		loginperform.usernametextboxname("JasonSanjay");
		loginperform.resetbutton();
		Assert.assertFalse(loginperform.isusernametextboxempty());
		
	}
	
	@Test
	public void j()
	{
		
		BaseClass10 loginperform = new BaseClass10();
		PageFactory.initElements(driver, loginperform);
		loginperform.clickadminbutton();
		loginperform.clickusermanagement();
		loginperform.clickusers();
		loginperform.clickadd();
		Assert.assertTrue(loginperform.adduserpage());
		
	}
	
	@Test
	public void k()
	{
		
		BaseClass11 loginperform = new BaseClass11();
		PageFactory.initElements(driver, loginperform);
		loginperform.clickadminbutton();
		loginperform.clickusermanagement();
		loginperform.clickusers();
		loginperform.clickedit();
		Assert.assertTrue(loginperform.edituserpage());
		
	}
	
	@Test
	public void l()
	{
		
		BaseClass12 loginperform = new BaseClass12();
		PageFactory.initElements(driver, loginperform);
		loginperform.clickadminbutton();
		loginperform.clickusermanagement();
		loginperform.clickusers();
		loginperform.clickcheckbox();
		Assert.assertTrue(loginperform.deleteselecteddisplayed());
		
	}
	
	
	@Test
	public void n()
	{
		
		BaseClass14 loginperform = new BaseClass14();
		PageFactory.initElements(driver, loginperform);
		loginperform.clickadminbutton();
		loginperform.clickusermanagement();
		loginperform.clickusers();
		loginperform.profilebutton();
		loginperform.logoutlink();
		Assert.assertTrue(loginperform.loginpageisdispayed());
		
	}

	@AfterTest
	void closebrowser()
	{
		driver.close();
	}
}
