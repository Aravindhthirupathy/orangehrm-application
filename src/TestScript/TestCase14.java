package TestScript;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import BaseClass.BaseClass14;

public class TestCase14 {
	
	static {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver_win32\\chromedriver.exe");
	}
	WebDriver driver;
	
@Test
	
	public void loginaction()
	{
		
		BaseClass14 loginperform = new BaseClass14();
		PageFactory.initElements(driver, loginperform);
		loginperform.clickadminbutton();
		loginperform.clickusermanagement();
		loginperform.clickusers();
		loginperform.profilebutton();
		loginperform.logoutlink();
		Assert.assertTrue(loginperform.loginpageisdispayed());
		driver.close();
		
	}




}
