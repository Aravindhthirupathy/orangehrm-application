package TestScript;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import BaseClass.BaseClass11;

public class TestCase11 {
	
	static {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver_win32\\chromedriver.exe");
	}
	WebDriver driver;
	
@Test
	
	public void loginaction()
	{
		
		
		BaseClass11 loginperform = new BaseClass11();
		PageFactory.initElements(driver, loginperform);
		loginperform.clickadminbutton();
		loginperform.clickusermanagement();
		loginperform.clickusers();
		loginperform.clickedit();
		Assert.assertTrue(loginperform.edituserpage());
		driver.close();
		
	}


}
