package TestScript;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import BaseClass.BaseClass10;

public class TestCase10 {
	
	static {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver_win32\\chromedriver.exe");
	}
	
	WebDriver driver;
	
@Test
	
	public void loginaction()
	{
		
		
		BaseClass10 loginperform = new BaseClass10();
		PageFactory.initElements(driver, loginperform);
		loginperform.clickadminbutton();
		loginperform.clickusermanagement();
		loginperform.clickusers();
		loginperform.clickadd();
		Assert.assertTrue(loginperform.adduserpage());
		driver.close();
		
	}


}
