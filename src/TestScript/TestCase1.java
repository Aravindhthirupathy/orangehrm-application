package TestScript;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import BaseClass.BaseClass1;

public class TestCase1 {
	
	static {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver_win32\\chromedriver.exe");
	}
	
	WebDriver driver;
	
	@Test
	
	public void loginaction()
	{
		BaseClass1 loginperform = new BaseClass1();
		PageFactory.initElements(driver, loginperform);
		Assert.assertTrue(loginperform.dashboarddisplayed());
		driver.close();
		
	}

}
