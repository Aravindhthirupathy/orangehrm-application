package TestScript;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import BaseClass.BaseClass8;

public class TestCase8 {
	
	static {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver_win32\\chromedriver.exe");
	}
	WebDriver driver;
	
	@Test
	
	public void loginaction()
	{
		driver= new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");
		
		BaseClass8 loginperform = new BaseClass8();
		PageFactory.initElements(driver, loginperform);
		loginperform.clickadminbutton();
		loginperform.clickusermanagement();
		loginperform.uparrowclick();
		Assert.assertTrue(loginperform.alloptionsminimized());
		driver.close();
		
		
	}


}
