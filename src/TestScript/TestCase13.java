package TestScript;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import BaseClass.BaseClass13;

public class TestCase13 {
	
	WebDriver driver;
	
	static {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver_win32\\chromedriver.exe");
	}
	
@Test
	
	public void loginaction()
	{
		driver= new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");
		
		BaseClass13 loginperform = new BaseClass13();
		PageFactory.initElements(driver, loginperform);
		loginperform.enterusername("Admin");
		loginperform.enterpassword("admin123");
		loginperform.clickloginbutton();
		loginperform.clickadminbutton();
		loginperform.clickusermanagement();
		loginperform.clickusers();
		loginperform.orangehrmlinkclick();
		ChromeOptions options= new ChromeOptions();
		options.addArguments("disable-notifications");
		driver.get("https://www.orangehrm.com/");
		Assert.assertTrue(loginperform.orangehrmlogodispayed());
		driver.close();
	}


}
