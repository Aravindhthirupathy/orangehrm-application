package TestScript;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import BaseClass.BaseClass6;

public class TestCase6 {
	
	static {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver_win32\\chromedriver.exe");
	}
	WebDriver driver;
@Test
	
	public void loginaction()
	{
		
		
		BaseClass6 loginperform = new BaseClass6();
		PageFactory.initElements(driver, loginperform);
		loginperform.clickadminbutton();
		loginperform.clickusermanagement();
		loginperform.clickusers();
		loginperform.employeenametextboxname("Anthony Nolan");
		loginperform.clickoption();
		loginperform.searchbutton();
		Assert.assertTrue(loginperform.onerecordwasfound());
		driver.close();
		
	}


}
