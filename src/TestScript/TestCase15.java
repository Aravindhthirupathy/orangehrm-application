package TestScript;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import BaseClass.BaseClass13;
import BaseClass.BaseClass2;
import BaseClass.BaseClass7;

public class TestCase15 {
	
	WebDriver driver;
	
	static {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver_win32\\chromedriver.exe");
	}
	
	@Test
	public void b()
	{
		driver= new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");
		
		BaseClass2 loginperform = new BaseClass2();
		PageFactory.initElements(driver, loginperform);
		loginperform.enterusername("Admi");
		loginperform.enterpassword("admin123");
		loginperform.clickloginbutton();
		Assert.assertTrue(loginperform.invalidcredentialdisplayed());
		driver.close();
	}
	
	@Test
	public void m()
	{
		driver= new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");
		
		BaseClass13 loginperform = new BaseClass13();
		PageFactory.initElements(driver, loginperform);
		loginperform.enterusername("Admin");
		loginperform.enterpassword("admin123");
		loginperform.clickloginbutton();
		loginperform.clickadminbutton();
		loginperform.clickusermanagement();
		loginperform.clickusers();
		loginperform.orangehrmlinkclick();
		ChromeOptions options= new ChromeOptions();
		options.addArguments("disable-notifications");
		driver.get("https://www.orangehrm.com/");
		Assert.assertTrue(loginperform.orangehrmlogodispayed());
		driver.close();
		
		
	}
	
	

}
